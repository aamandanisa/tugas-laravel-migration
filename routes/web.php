<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome','AuthController@send');
Route::get('/data-table',function(){
    return view('halaman.datatable');
});

//CRUD Cast
//Create Data
//mengarahkan ke form tambah cast
Route::get('/cast/create','CastController@create');

// Menyimpan data ke database
Route::post('/cast','CastController@store');

//Read Data
//route untuk menampilkan semua data di database
Route::get('/cast','CastController@index');
//Route Detail Biodata
Route::get('/cast/{cast_id}','CastController@show');
//Update Data
//Mengarah ke form edit cast yang membawa parameter
Route::get('/cast/{cast_id}/edit','CastController@edit');
//Update data di table cast berdasarkan id
Route::put('/cast/{cast_id}','CastController@update');
//Delete Data
//menghapus data di cast berdasarkan id
Route::delete('/cast/{cast_id}','CastController@destroy');