<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h2>Input Form</h2>
<form action="/welcome" method="post">
    @csrf
    <label>Full name:</label><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="gender" value="1">Male<br>
    <input type="radio" name="gender" value="2">Female<br><br>
    <label>Nationality:</label><br>
    <select name="country">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapore">Singapore</option>
    </select><br><br>
    <label>Language spoken</label><br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br><br>
    <label>Bio</label><br>
    <textarea name="bio" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" value="Sign Up">


</form>
</body>
</html>